﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Globalization;
using System.Windows.Markup;
using ProtocolTester.ViewModel;
using System.IO;

namespace ProtocolTester
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Main App
        /// </summary>
        static App ( )
        {
            // This code is used to test the app when using other cultures.
            //
            //System.Threading.Thread.CurrentThread.CurrentUICulture =
            //    System.Threading.Thread.CurrentThread.CurrentUICulture =
            //    new System.Globalization.CultureInfo ( "it-IT" );

            // Ensure the current culture passed into bindings is the OS culture.
            // By default, WPF uses en-US as the culture, regardless of the system settings.
            //
            FrameworkElement.LanguageProperty.OverrideMetadata (
                typeof ( FrameworkElement ),
                new FrameworkPropertyMetadata ( XmlLanguage.GetLanguage ( CultureInfo.CurrentCulture.IetfLanguageTag ) ) );
        }

        /// <summary>
        /// Override Startup event to show default view and viewmodel.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup ( StartupEventArgs e )
        {
            base.OnStartup ( e );

            // Create default view.
            MainWindow window = new MainWindow ( );

            // Create default viewmodel.
            var viewModel = new MainViewModel ( );

            // when the ViewModel asks to be closed,
            // close the window.
            EventHandler handler = null;
            handler = delegate
            {
                viewModel.RequestClose -= handler;
                window.Close ( );
            };
            viewModel.RequestClose += handler;

            // Allow all controls in the window to
            // bind to the ViewModel by setting the 
            // DataContext, which propagates down 
            // the element tree.
            window.DataContext = viewModel;
            
            window.Show ( );
        }
    }
}
