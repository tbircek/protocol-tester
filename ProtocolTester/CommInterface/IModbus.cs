﻿using System;

namespace ProtocolTester.Communication
{
    /// <summary>
    /// Provides interface for the Modbus Protocol.
    /// </summary>
    public interface IModbus
    {
        /// <summary>
        /// Retrieves DeviceId value from the units
        /// </summary>
        /// <param name="ipAddress">The unit ip address to connect.</param>
        /// <param name="portNumber">The unit port number to connect.</param>
        /// <param name="protocolRegister">The unit modbus protocolRegister number that holds the DeviceId value.</param>
        /// <returns>Returns "DeviceId" value if connection is made and response is acceptable 
        /// otherwise returns "-1".</returns>
        Int32 GetDeviceId ( string ipAddress, string portNumber, string protocolRegister );               

        /// <summary>
        /// Provides write function to Modbus protocol.
        /// </summary>
        /// <param name="registerUnderTest">Modbus register to test. BecoViewModel will have every available info about the register.</param>
        void Write ( ProtocolTester.ViewModel.BecoViewModel registerUnderTest );

        /// <summary>
        /// Provides read function to Modbus protocol.
        /// </summary>
        /// <param name="registerUnderTest">Modbus register to test. BecoViewModel will have every available info about the register.</param>
        void Read ( ProtocolTester.ViewModel.BecoViewModel registerUnderTest );

    }
}
