﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtocolTester.Model;

namespace ProtocolTester.DataAccess
{
    /// <summary>
    /// Event arguments used by BecoUnitRepository's BecoUnitAdded event.
    /// </summary>
    public class BecoUnitAddedEventArgs: EventArgs
    {
        public BecoUnitAddedEventArgs (Beco newBecoUnit)
        {
            this.NewBecoUnit = newBecoUnit;
        }

        public Beco NewBecoUnit { get; private set; }
    }
}
