﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Linq;
using ProtocolTester.Model;
using ProtocolTester.Properties;

namespace ProtocolTester.DataAccess
{
    /// <summary>
    /// Represents a source of becounits in the application.
    /// </summary>
    public class BeckwithRepository
    {
        #region Fields

        readonly List<Beco> becoUnits;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Default construct.
        /// </summary>
        public BeckwithRepository ( )
        {
            becoUnits = new List<Beco> ( );
        }

        /// <summary>
        /// returns beco unit value which is everything save so far.
        /// </summary>
        /// <param name="becoUnit">the current beco unit.</param>
        public BeckwithRepository ( Beco becoUnit )
        {
            becoUnits = LoadBecoUnits ( becoUnit );
        }

        #endregion // Constructors

        #region Public Interface

        /// <summary>
        /// Raised when a beco unit is placed into the repository.
        /// </summary>
        public event EventHandler<BecoUnitAddedEventArgs> BecoAdded;

        /// <summary>
        /// Places the specified beco unit into the repository.
        /// If the beco unit is already in the repository, an 
        /// exception is NOT thrown.
        /// </summary>
        public void AddBecoUnit ( Beco becoUnit )
        {
            if ( becoUnit == null )
                throw new ArgumentNullException ( "becoUnit" );

            if ( !becoUnits.Contains ( becoUnit ) )
            {
                // Grab ipAddress to use.
                UnitIPAddress = becoUnit.IPAddress;

                // Grab portNumber to use.
                PortNumber = becoUnit.PortNumber;

                Debug.WriteLine ( string.Format (
                                                    CultureInfo.InvariantCulture,
                                                    "\n\nDevice id: {0}\nIPAddress: {1}\nPort number: {2}\nProtocol: {3}\nRegister: {4}\n\nBecounit added",
                                                    becoUnit.DeviceId,
                                                    becoUnit.IPAddress,
                                                    becoUnit.PortNumber,
                                                    becoUnit.Protocol,
                                                    becoUnit.Address
                                                 )
                                 );

                IList<Beco> newUnit = LoadBecoUnits ( becoUnit );

                // load every protocolRegister to the memory.
                foreach ( var item in newUnit )
                    becoUnits.Add ( item );

                if ( this.BecoAdded != null )
                    this.BecoAdded ( this, new BecoUnitAddedEventArgs ( becoUnit ) );
            }
        }

        /// <summary>
        /// Returns true if the specified beco unit exists in the 
        /// repository, or false if it is not.
        /// </summary>
        public bool ContainsBecoUnit ( Beco becoUnit )
        {
            if ( becoUnit == null )
                throw new ArgumentNullException ( "becoUnit" );

            if ( becoUnits == null || becoUnits.Count == 0 )
                return false;

            bool isSubset = true;

            foreach ( var element in becoUnits )
            {
                // if following parameters same, we are talking about same unit with same test.
                if ( becoUnit.DeviceId == element.DeviceId &&
                     becoUnit.IPAddress == element.IPAddress &&
                     becoUnit.PortNumber == element.PortNumber &&
                     becoUnit.Protocol == element.Protocol )
                {
                    isSubset = true;
                    break;
                }
                else
                {
                    isSubset = false;
                    break;
                }
            }

            return isSubset;
        }

        /// <summary>
        /// Returns a shallow-copied list of all beco units in the repository.
        /// </summary>
        public List<Beco> GetBecoUnits ( )
        {

            return new List<Beco> ( becoUnits );

        }

        /// <summary>
        /// Holds new beco ip address.
        /// </summary>
        public string UnitIPAddress { get; set; }

        /// <summary>
        /// Holds new beco port number.
        /// </summary>
        public string PortNumber { get; set; }

        /// <summary>
        /// Holds new beco deviceid.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Holds new beco protocol.
        /// </summary>
        public string Protocol { get; set; }

        #endregion // Public Interface

        #region Private Helpers

        /// <summary>
        /// Creates default and subsequent Beco devices.
        /// </summary>
        /// <param name="becoDataFile">XML file holds values for the devices.</param>
        /// <returns></returns>
        static List<Beco> LoadBecoUnits ( Beco becoUnit )
        {

            string becoDataFile = "";
            string elementName = "";
            string elementsName = "register";

            switch ( becoUnit.DeviceId )
            {
                case "214":
                    elementName = "m2001d";
                    break;

                case "228":
                    elementName = "m6200a";
                    break;

                case "254":
                    elementName = "m6280a";
                    break;

                default:
                    // default to use m2001d modbus document.
                    elementName = "m2001d";
                    break;
            }

#if DEBUG
            becoDataFile = string.Format ( CultureInfo.InvariantCulture,
                                           "pack://application:,,,/data/{0}/modbus/debug.xml",
                                            elementName
                                          );
#else
            becoDataFile = string.Format ( CultureInfo.InvariantCulture,
                                          "pack://application:,,,/data/{0}/modbus/{0}.xml",
                                          elementName 
                                          ); 
#endif

            Debug.WriteLine ( string.Format (
                                            CultureInfo.InvariantCulture,
                                            "\nBeco data file: {0}\nElement Name: {1}\n",
                                            becoDataFile,
                                            elementName
                                            )
                            );

            using ( Stream stream = GetElementStream ( becoDataFile ) )

            using ( XmlReader xmlrdr = new XmlTextReader ( stream ) )
                return
                    ( from customerElem in XDocument.Load ( xmlrdr ).Element ( elementName ).Elements ( elementsName )
                      select Beco.CreateBecoUnit
                      (
                          becoUnit.DeviceId,
                          becoUnit.IPAddress,
                          becoUnit.PortNumber,
                          becoUnit.Protocol,
                          ( string ) customerElem.Element ( "address" ),
                          ( string ) customerElem.Element ( "writeable" ),
                          ( string ) ( ( double ) customerElem.Element ( "minimum" ) / ( double ) customerElem.Element ( "increment" ) ).ToString ( CultureInfo.InvariantCulture ),
                          ( string ) ( ( double ) customerElem.Element ( "maximum" ) / ( double ) customerElem.Element ( "increment" ) ).ToString ( CultureInfo.InvariantCulture ),
                          ( int ) ( ( double ) customerElem.Element ( "minimum" ) / ( double ) customerElem.Element ( "increment" ) ),
                          Strings.NotTested
                       )
                    )
                    .ToList ( );
        }

        private static Stream GetElementStream ( string resourceFile )
        {
            Uri uri = new Uri ( resourceFile, UriKind.Absolute );

            Debug.WriteLine ( uri.LocalPath );

            StreamResourceInfo info = Application.GetContentStream ( uri );

            if ( info == null || info.Stream == null )
                throw new ArgumentException ( "Missing resource file: " + resourceFile );

            return info.Stream;
        }

        #endregion // Private Helpers

    }
}
