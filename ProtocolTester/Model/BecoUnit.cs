﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ProtocolTester.Communication;
using ProtocolTester.Properties;
using ProtocolTester.Protocols;

namespace ProtocolTester.Model
{
    /// <summary>
    /// Represents a unit. This class
    /// has built-in validation logic. It is wrapped
    /// by the BecoViewModel class, which enables it to
    /// be easily displayed and edited by a WPF user interface.
    /// </summary>
    public sealed class Beco : IDataErrorInfo
    {
        #region New Unit

        /// <summary>
        /// Handles new unit.
        /// </summary>
        /// <returns>Return new beco unit.</returns>
        public static Beco CreateNewBecoUnit ( )
        {
            return new Beco ( );
        }

        /// <summary>
        /// Creates a new unit from xml file.
        /// </summary>
        /// <param name="deviceId">Device ID read value from the unit.</param>
        /// <param name="ipAddress">from the user input textbox.</param>
        /// <param name="portNumber">from the user input textbox.</param>
        /// <param name="protocol">selected by user. Not sure about data type of the necessity of this parameter.</param>
        /// <param name="address">protocolRegister address (modbus). document input.</param>
        /// <param name="writable">protocolRegister read or write . document input.</param>
        /// <param name="minimum">minimum value accepted by the protocolRegister. document input.</param>
        /// <param name="maximum">maximum value accepted by the protocolRegister. document input.</param>
        /// <returns></returns>
        public static Beco CreateBecoUnit (
                string deviceId,
                string ipAddress,
                string portNumber,
                string protocol,
                string address,
                string writable,
                string minimum,
                string maximum,
                int percentage,
                string result
            )
        {
            return new Beco
            {
                DeviceId = deviceId,
                IPAddress = ipAddress,
                PortNumber = portNumber,
                Protocol = protocol,
                Address = address,
                Writable = writable,
                Minimum = minimum,
                Maximum = maximum,
                Percentage = percentage,
                Result = result
            };
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //protected Beco ( ) { }

        #endregion // Creation

        #region State Properties

        /// <summary>
        /// Gets/sets DeviceId.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Gets/sets ipAddress.
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// Gets/sets Port number.
        /// </summary>
        public string PortNumber { get; set; }

        /// <summary>
        /// Gets/sets Protocol.
        /// </summary>
        public string Protocol { get; set; }

        /// <summary>
        /// Gets/sets Address of the protocolRegister.
        /// </summary>
        public string Address { get; set; }

        ///// <summary>
        ///// Gets/sets whether Modbus protocol test or not.
        ///// The default value is true.
        ///// </summary>
        //public bool IsModbus { get; set; }

        ///// <summary>
        ///// Gets/sets whether DNP3.0 protocol test or not.
        ///// The default value is false.
        ///// </summary>
        //public bool IsDNP30 { get; set; }

        ///// <summary>
        ///// Gets/sets whether IEC61850 protocol test or not.
        ///// The default value is false.
        ///// </summary>
        //public bool IsIEC61850 { get; set; }

        /// <summary>
        /// Gets/sets whether the protocolRegister Writable or not.
        /// The default is false.
        /// </summary>
        public string Writable { get; set; }

        /// <summary>
        /// Gets/sets whether the minimum value accepted by the protocolRegister.
        /// </summary>
        public string Minimum { get; set; }

        /// <summary>
        /// Gets/sets whether the maximum value accepted by the protocolRegister.
        /// </summary>
        public string Maximum { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public string Progress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Percentage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Result { get; set; }

        ///// <summary>
        ///// Gets/sets whether the Access level of the protocolRegister.
        ///// </summary>
        //public string Access { get; set; }

        #endregion // State Properties

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        string IDataErrorInfo.this[ string propertyName ]
        {
            get { return this.GetValidationError ( propertyName ); }
        }

        #endregion // IDataErrorInfo Members

        #region Validation

        /// <summary>
        /// Returns true if this object has no validation errors.
        /// </summary>
        public bool IsValid
        {
            get
            {
                foreach ( string property in ValidatedProperties )
                    if ( GetValidationError ( property ) != null )
                        return false;

                return true;
            }
        }

        /// <summary>
        /// add property to be validated here.
        /// </summary>
        static readonly string[] ValidatedProperties =
        {
            "IPAddress",
            "PortNumber",
            "DeviceId",
            // "NewUnitAdded",
            "ProtocolOptions"
        };

        /// <summary>
        /// updates validation results for all validated properties.
        /// </summary>
        string GetValidationError ( string propertyName )
        {
            if ( Array.IndexOf ( ValidatedProperties, propertyName ) < 0 )
                return null;

            string error = null;

            switch ( propertyName )
            {
                case "IPAddress":
                    error = this.ValidateIPAddress ( );
                    break;

                case "PortNumber":
                    error = this.ValidatePortNumber ( );
                    break;

                case "DeviceId":
                    Debug.WriteLine ( " Running DeviceId" );
                    error = this.ValidateDeviceID ( );
                    break;

                //case "NewUnitAdded":
                //    error = null;
                //    break;

                case "ProtocolOptions":
                    error = this.ValidateCommOptions ( );
                    break;

                default:
                    Debug.Fail ( "Unexpected property being validated: " + propertyName );
                    break;
            }

            return error;
        }

        private string ValidateDeviceID ( )
        {
            if ( !IsSupportedDevice ( ) )
            {
                return Strings.DeviceIdNotSupported;
            }
            return null;
        }

        private string ValidateIPAddress ( )
        {
            if ( IsStringMissing ( this.IPAddress ) )
            {
                return Strings.Error_IpAddressMissing;
            }
            else if ( !IsValidIpAddress ( ) )
            {
                return Strings.Error_InvalidIPAddress;
            }
            else if ( !Task.Factory.StartNew ( ( ) => IsPingable ( ) ).Result )
            {
                return Strings.Error_NoPing;
            }
            //if ( !IsSupportedDevice ( ) )
            //{
            //    return Strings.DeviceID_NotSupported;
            //}

            return null;
        }

        private string ValidatePortNumber ( )
        {
            if ( IsStringMissing ( this.PortNumber ) )
            {
                return Strings.MissingPortNumber;
            }
            else if ( !IsPortNumber ( this.PortNumber ) )
            {
                return Strings.Error_NotValid;
            }
            //if ( !IsSupportedDevice ( ) )
            //{
            //    return Strings.DeviceID_NotSupported;
            //}

            return null;
        }

        private string ValidateCommOptions ( )
        {
            if ( !this.IsChecked ( ) )
            {
                return Strings.NotSpecified;
            }
            return null;
        }

        private static bool IsStringMissing ( string value )
        {
            return
                String.IsNullOrEmpty ( value );
        }

        private static bool IsPortNumber ( string portnumber )
        {
            try
            {
                if(string.IsNullOrWhiteSpace(portnumber ))
                    return false;

                string pattern = @"^([1-9]\d{0,3}|[1-5]\d{4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5])$";

                Regex PortNumber = new Regex (
                                                pattern,
                                                RegexOptions.Compiled
                                                );
                return PortNumber.IsMatch ( portnumber );
            }
            catch ( ArgumentException e )
            {
                Debug.WriteLine ( e.Message );
                return false;
            }
        }

        private bool IsValidIpAddress ( )
        {
            try
            {
                string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";

                Regex ipAddressRegex = new Regex (
                                                pattern,
                                                RegexOptions.Compiled
                                                );

                return ipAddressRegex.IsMatch ( this.IPAddress );
            }
            catch ( ArgumentNullException e )
            {
                Debug.WriteLine ( e.Message );
                return false;
            }
        }

        private bool IsPingable ( )
        {
            try
            {
                const int timeout = 50;     // msec timeout for ping
                byte[] buffer = Encoding.ASCII.GetBytes ( "aaaaaBeckwithaaaaaa" );    // data to send in ping message in case I need to follow this ping.

                Ping ping = new Ping ( );
                PingOptions options = new PingOptions ( );

                options.DontFragment = true;
                options.Ttl = 125;

                PingReply reply = Task.Factory.StartNew ( ( ) => ping.Send ( this.IPAddress, timeout, buffer, options ) ).Result;

                return reply.Status == IPStatus.Success;
            }
            catch ( PingException p )
            {
                Debug.WriteLine ( p.Message );
                return false;
            }
        }

        private bool IsChecked ( )
        {
            // Return true if any or all checked
            return true;

            // Return false if none checked
            // return false;
        }

        private bool IsSupportedDevice ( )
        {
            IModbus modbus = new Modbus ( );

            Int32 device = Task.Factory.StartNew ( ( ) =>  modbus.GetDeviceId ( this.IPAddress, this.PortNumber, "700" )).Result;

            if ( device == -1 )
            {
                return false;
            }
            else
            {
                this.DeviceId = Convert.ToString ( device, CultureInfo.InvariantCulture );
                return true; 
            }
            //CEthernet ethernet = new CEthernet ( );

            //uint ip = ConvertToUInt32 ( );
            //int port = Convert.ToInt32 ( this.portNumber );

            //if ( !ethernet.Open ( ip, port, port ) )
            //{
            //    Debug.WriteLine ( "ID failed --- " );
            //    return false;
            //}
            //else
            //{
            //    CProtocol protocol = new CModbusProtocol ( );

            //    ( ( CModbusProtocol ) protocol ).EanbleMBAP ( true );

            //    protocol.BindConnection ( ethernet );
            //    protocol.SetAddress ( 0xff );
            //    protocol.SetLimitCountForReadWrite ( 1, 1 );

            //    CCommDataFlow dataFlow = new CCommDataFlow ( );
            //    dataFlow.Protocol = protocol;
            //    dataFlow.Connection = ethernet;
            //    dataFlow.OnInitialize ( CDataSet.DataSet );

            //    DeviceId = Convert.ToString ( Task.Factory.StartNew ( ( ) => dataFlow.LoadData ( 700 ) ).Result );

            //    Debug.WriteLine ( "ID is : " + DeviceId );

            //    dataFlow.Close ( );

            //    return true;
            //}
        }

        ///// <summary>
        ///// Converts a string to a uInt32.
        ///// </summary>
        ///// <returns>Returns a uInt32 value converted in a big-endian</returns>
        //private uint ConvertToUInt32 ( )
        //{
        //    if ( this.IPAddress == null )
        //        return 0;

        //    return BitConverter.ToUInt32 (
        //                  System.Net.IPAddress.Parse ( this.IPAddress ) // convert string to IPAddress
        //                        .GetAddressBytes ( )                    // convert IPAddress to byte[]
        //                        .Reverse ( )                            // Reverse array to get big-endian
        //                        .ToArray ( )                            // Generate a new byte[] array
        //                        , 0                                     // from statingIndex == 0
        //                        );


        //}

        #endregion // Validation
    }
}
