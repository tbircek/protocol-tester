﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Communication;
using DeviceDataSet;
using ProtocolTester.Communication;
using System.Windows.Threading;
using System.Windows;
using ProtocolTester.Properties;

namespace ProtocolTester.Protocols
{
    /// <summary>
    /// Provides access to Modbus Communication API.
    /// </summary>
    public sealed class Modbus : IModbus
    {

        #region private properties

        private string IpAddress { get; set; }

        private string PortNumber { get; set; }

        #endregion

        #region IModbus Members

        #region GetDeviceId

        /// <summary>
        /// Retrieves DeviceId value from the units
        /// </summary>
        /// <param name="ipAddress">The unit ip address to connect.</param>
        /// <param name="portNumber">The unit port number to connect.</param>
        /// <param name="protocolRegister">The unit modbus protocolRegister number that holds the DeviceId value.</param>
        /// <returns>Returns "DeviceId" value if connection is made and response is acceptable 
        /// otherwise returns "-1".</returns>
        public Int32 GetDeviceId ( string ipAddress, string portNumber, string protocolRegister )
        {
            if ( string.IsNullOrWhiteSpace ( ipAddress ) )
                return -1;

            if ( string.IsNullOrWhiteSpace ( portNumber ) )
                return -1;

            this.IpAddress = ipAddress;
            this.PortNumber = portNumber;

            CEthernet ethernet = new CEthernet ( );

            uint ip = ConvertToUInt32 ( );
            int port = Convert.ToInt32 ( this.PortNumber, CultureInfo.InvariantCulture );

            if ( !ethernet.Open ( ip, port, port ) )
            {
                Debug.WriteLine ( "ID failed --- " );
                return -1;
            }
            else
            {
                CProtocol protocol = new CModbusProtocol ( );

                ( ( CModbusProtocol ) protocol ).EanbleMBAP ( true );

                protocol.BindConnection ( ethernet );
                protocol.SetAddress ( 0xff );
                protocol.SetLimitCountForReadWrite ( 1, 1 );

                CCommDataFlow dataFlow = new CCommDataFlow ( );
                dataFlow.Protocol = protocol;
                dataFlow.Connection = ethernet;
                dataFlow.OnInitialize ( CDataSet.DataSet );

                Int32 DeviceId = Task.Factory.StartNew ( ( ) => dataFlow.LoadData ( 700 ) ).Result;

                Debug.WriteLine ( "ID is : " + DeviceId );

                dataFlow.Close ( );

                return DeviceId;
            }
        }

        #endregion

        #region Read

        /// <summary>
        /// Provides read function to Modbus protocol.
        /// </summary>
        /// <param name="registerUnderTest">Modbus register to test. BecoViewModel will have every available info about the register.</param>
        public void Read ( ViewModel.BecoViewModel registerUnderTest )
        {
            if ( string.IsNullOrWhiteSpace ( registerUnderTest.IPAddress ) )
                return; // return 0xfffffe;

            if ( string.IsNullOrWhiteSpace ( registerUnderTest.PortNumber ) )
                return; // return 0xffffff;

            if ( string.IsNullOrWhiteSpace ( registerUnderTest.Address ) )
                return; // return 0xfffffd;

            this.IpAddress = registerUnderTest.IPAddress;
            this.PortNumber = registerUnderTest.PortNumber;

            CEthernet ethernet = new CEthernet ( );

            uint ip = ConvertToUInt32 ( );
            int port = Convert.ToInt32 ( PortNumber, CultureInfo.InvariantCulture );

            if ( !ethernet.Open ( ip, port, port ) )
            {
                Debug.WriteLine ( "ID failed --- " );
                return; // return 0xfffffc;
            }
            else
            {
                CProtocol protocol = new CModbusProtocol ( );

                ( ( CModbusProtocol ) protocol ).EanbleMBAP ( true );

                protocol.BindConnection ( ethernet );
                protocol.SetAddress ( 0xff );
                protocol.SetLimitCountForReadWrite ( 1, 1 );

                CCommDataFlow dataFlow = new CCommDataFlow ( );
                dataFlow.Protocol = protocol;
                dataFlow.Connection = ethernet;
                dataFlow.OnInitialize ( CDataSet.DataSet );

                try
                {
                    // Queue<Exception> parallelLoopExceptions = new Queue<Exception> ( );
                    ParallelOptions parallelLoopOptions = new ParallelOptions
                    {
                        // CancellationToken = someTokeHere,
                        MaxDegreeOfParallelism = Environment.ProcessorCount //,
                        // TaskScheduler = someTaskScheduler
                    };

                    ParallelLoopResult loopResult = Parallel.For (
                    0,
                    1, // testing one testRegister's range. Ex: 701 range 0-65535
                    parallelLoopOptions,
                    ( readValue, loopState ) =>
                    {

                        readValue = Task.Factory.StartNew (
                                            ( ) =>
                                                dataFlow.LoadData
                                                    ( Convert.ToInt32 ( registerUnderTest.Address, CultureInfo.InvariantCulture ) ) )
                                                        .Result;

                        registerUnderTest.Result = registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, "Value: {0}", readValue );

                        registerUnderTest.Percentage = Convert.ToInt32 ( registerUnderTest.Maximum, CultureInfo.InvariantCulture );

                        // see if cancellation requested.
                        if ( loopState.ShouldExitCurrentIteration )
                            return;

                    } );

                    if ( loopResult.IsCompleted )
                    {


                        //string msg = string.Format ( CultureInfo.InvariantCulture,
                        //                             "{2}: Reading Register: {0} value: {1} completed.",
                        //                             registerUnderTest.Address,
                        //                             registerUnderTest.Result,
                        //                             DateTime.Now.TimeOfDay
                        //                             );

                        Debug.WriteLine ( string.Format ( CultureInfo.InvariantCulture,
                                                          "{2}: Reading Register: {0} value: {1} completed.",
                                                          registerUnderTest.Address,
                                                          registerUnderTest.Result,
                                                          DateTime.Now.TimeOfDay
                                                         )
                                         );
                    }
                }

                catch ( AggregateException e )
                {
                    //string msg = string.Format (
                    //                            CultureInfo.InvariantCulture,
                    //                            "ModbusComm",
                    //                            e
                    //                            );
                    Debug.WriteLine ( string.Format (
                                                CultureInfo.InvariantCulture,
                                                "ModbusComm",
                                                e
                                                )
                                    );
                }
                catch ( OperationCanceledException e )
                {
                    //string msg = string.Format (
                    //                            CultureInfo.InvariantCulture,
                    //                            "ModbusComm",
                    //                            e
                    //                            );
                    Console.WriteLine ( string.Format (
                                                CultureInfo.InvariantCulture,
                                                "ModbusComm",
                                                e
                                                ) 
                                       );
                }

                dataFlow.Close ( );

                // return readValue;
            }
        }

        #endregion

        #region Write

        /// <summary>
        /// Provides write function to Modbus protocol.
        /// </summary>
        /// <param name="registerUnderTest">Modbus register to test. BecoViewModel will have every available info about the register.</param>
        public void Write ( ViewModel.BecoViewModel registerUnderTest )
        {
            if ( string.IsNullOrWhiteSpace ( registerUnderTest.IPAddress ) )
                return;

            if ( string.IsNullOrWhiteSpace ( registerUnderTest.PortNumber ) )
                return;

            if ( string.IsNullOrWhiteSpace ( registerUnderTest.Address ) )
                return;

            this.IpAddress = registerUnderTest.IPAddress;
            this.PortNumber = registerUnderTest.PortNumber;

            CEthernet ethernet = new CEthernet ( );

            uint ip = ConvertToUInt32 ( );
            int port = Convert.ToInt32 ( PortNumber, CultureInfo.InvariantCulture );

            if ( !ethernet.Open ( ip, port, port ) )
            {
                Debug.WriteLine ( "ID failed --- " );
                return;
            }
            else
            {
                CProtocol protocol = new CModbusProtocol ( );

                ( ( CModbusProtocol ) protocol ).EanbleMBAP ( true );

                protocol.BindConnection ( ethernet );
                protocol.SetAddress ( 0xff );
                protocol.SetLimitCountForReadWrite ( 1, 1 );

                CCommDataFlow dataFlow = new CCommDataFlow ( );
                dataFlow.Protocol = protocol;
                dataFlow.Connection = ethernet;
                dataFlow.OnInitialize ( CDataSet.DataSet );

                try
                {
                    //if ( modbusFunction == ModbusFunctions.Write )
                    //{

                    bool result = false;

                    // Assign a none modbus value.
                    // int errorValue = 65536;
                    string errorValue = null;

                    // Queue<Exception> parallelLoopExceptions = new Queue<Exception> ( );
                    ParallelOptions parallelLoopOptions = new ParallelOptions
                    {
                        // CancellationToken = someTokeHere,
                        MaxDegreeOfParallelism = Environment.ProcessorCount //,
                        // TaskScheduler = someTaskScheduler
                    };

                    ParallelLoopResult loopResult = Parallel.For (
                    0,
                    1, // testing one testRegister's range. Ex: 701 range 0-65535
                    parallelLoopOptions,
                    ( percent, loopState ) =>
                    {
                        for ( int value = Convert.ToInt32 ( registerUnderTest.Minimum, CultureInfo.InvariantCulture ) - 1; value < Convert.ToInt32 ( registerUnderTest.Maximum, CultureInfo.InvariantCulture ) + 2; value++ )
                        {

                            result = Task.Factory.StartNew (
                                                            ( ) =>
                                                                dataFlow.SaveData ( Convert.ToInt32 ( registerUnderTest.Address, CultureInfo.InvariantCulture ),
                                                                value ) )
                                                                .Result;


                            //string msg = string.Format ( CultureInfo.InvariantCulture,
                            //                             "{3}: Writing Register: {0} value: {1} write: {2} readValue: {4}\n\nPercentage is {5}",
                            //                             registerUnderTest.Address,
                            //                             value,
                            //                             result,
                            //                             DateTime.Now.TimeOfDay,
                            //                             percent,
                            //                             registerUnderTest.Percentage
                            //                            );

                            Debug.WriteLine ( string.Format ( CultureInfo.InvariantCulture,
                                                             "{3}: Writing Register: {0} value: {1} write: {2} readValue: {4}\n\nPercentage is {5}",
                                                             registerUnderTest.Address,
                                                             value,
                                                             result,
                                                             DateTime.Now.TimeOfDay,
                                                             percent,
                                                             registerUnderTest.Percentage
                                                            )
                                            );

                            registerUnderTest.Percentage = percent + Convert.ToInt32 ( registerUnderTest.Minimum, CultureInfo.InvariantCulture );

                            // see if cancellation requested.
                            if ( loopState.ShouldExitCurrentIteration )
                                return;

                            // Make a decision if the testRegister failures are expected.                                
                            if ( value > Convert.ToInt32 ( registerUnderTest.Minimum, CultureInfo.InvariantCulture ) &&
                                     value < Convert.ToInt32 ( registerUnderTest.Maximum, CultureInfo.InvariantCulture ) )
                            {
                                if ( !result )
                                {
                                    Debug.WriteLine ( "Failure recorded" );
                                    errorValue = value.ToString ( CultureInfo.InvariantCulture );
                                    // registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, Strings.AllBecosViewModel_ScoreBoard, value, percent );
                                }
                                else
                                {
                                    // registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, Strings.AllBecosViewModel_ScoreBoard, value, value );
                                }
                            }
                            else if ( value < Convert.ToInt32 ( registerUnderTest.Minimum, CultureInfo.InvariantCulture ) &&
                                     value > Convert.ToInt32 ( registerUnderTest.Maximum, CultureInfo.InvariantCulture ) )
                            {
                                // this good return NOT expected.
                                if ( result )
                                {
                                    Debug.WriteLine ( "Failure recorded" );
                                    errorValue = value.ToString ( CultureInfo.InvariantCulture );
                                    // registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, Strings.AllBecosViewModel_ScoreBoard, value, value );
                                }
                                else
                                {
                                    // registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, Strings.AllBecosViewModel_ScoreBoard, value, value );
                                }
                            }

                            registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, Strings.ScoreBoard, value, errorValue );


                            // local variable to hold process progress. 
                            percent++;
                        }
                    }

                );

                    if ( loopResult.IsCompleted )
                    {
                        //string msg = string.Format (
                        //                        CultureInfo.InvariantCulture,
                        //                        "Register: {0} testing completed for ranges Min: {1}, Max:{2}",
                        //                        registerUnderTest.Address,
                        //                        registerUnderTest.Minimum,
                        //                        registerUnderTest.Maximum
                        //                        );
                        Debug.WriteLine ( string.Format (
                                                CultureInfo.InvariantCulture,
                                                "Register: {0} testing completed for ranges Min: {1}, Max:{2}",
                                                registerUnderTest.Address,
                                                registerUnderTest.Minimum,
                                                registerUnderTest.Maximum
                                                )
                                        );

                        if ( registerUnderTest.Result == Strings.NotTested ) // "--" )
                        {
                            registerUnderTest.Result = "Passed";
                        }
                    }
                    //}
                    //else if ( modbusFunction == ModbusFunctions.Read )
                    //{
                    //    readValue = Task.Factory.StartNew (
                    //                        ( ) =>
                    //                            dataFlow.LoadData
                    //                                ( Convert.ToInt32 ( registerUnderTest.Address, CultureInfo.InvariantCulture ) ) )
                    //                                    .Result;

                    //    string msg = string.Format ( CultureInfo.InvariantCulture,
                    //                                 "{2}: Reading Register: {0} value: {1}",
                    //                                 registerUnderTest.Address,
                    //                                 readValue,
                    //                                 DateTime.Now.TimeOfDay
                    //                                 );

                    //    Debug.WriteLine ( msg );

                    //    registerUnderTest.Percentage = Convert.ToInt32 ( registerUnderTest.Maximum, CultureInfo.InvariantCulture );
                    //    registerUnderTest.Result = string.Format ( CultureInfo.InvariantCulture, "Value: {0}", readValue );
                    //}


                }
                catch ( AggregateException e )
                {
                    //string msg = string.Format (
                    //                            CultureInfo.InvariantCulture,
                    //                            "ModbusComm",
                    //                            e
                    //                            );
                    Debug.WriteLine ( string.Format (
                                                CultureInfo.InvariantCulture,
                                                "ModbusComm",
                                                e
                                                ) 
                                    );
                }
                catch ( OperationCanceledException e )
                {
                    //string msg = string.Format (
                    //                            CultureInfo.InvariantCulture,
                    //                            "ModbusComm",
                    //                            e
                    //                            );
                    Console.WriteLine ( string.Format (
                                                CultureInfo.InvariantCulture,
                                                "ModbusComm",
                                                e
                                                )
                                      );
                }

                dataFlow.Close ( );

                //return result;
            }
        }

        #endregion

        #endregion

        #region private methods

        /// <summary>
        /// Converts a string to a uInt32.
        /// </summary>
        /// <returns>Returns a uInt32 value converted in a big-endian</returns>
        private uint ConvertToUInt32 ( )
        {
            if ( this.IpAddress == null )
                return 0;

            return BitConverter.ToUInt32 (
                          System.Net.IPAddress.Parse ( this.IpAddress ) // convert string to IPAddress
                                .GetAddressBytes ( )                    // convert IPAddress to byte[]
                                .Reverse ( )                            // Reverse array to get big-endian
                                .ToArray ( )                            // Generate a new byte[] array
                                , 0                                     // from statingIndex == 0
                                );
        }

        #endregion

    }
}
