﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ProtocolTester.Communication;
using ProtocolTester.DataAccess;
using ProtocolTester.Properties;
using ProtocolTester.Protocols;

namespace ProtocolTester.ViewModel
{
    /// <summary>
    /// Represents a container of BecoUnitViewModel objects
    /// that has support for staying synchronized with the
    /// BecoUnitViewModel. This class also provides information
    /// related to multiple selected beco units.
    /// </summary>
    public class AllBecoViewModel : WorkspaceViewModel
    {

        #region Fields

        // readonly Beco _beco;
        readonly BeckwithRepository _becoRepository;

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="becoRepository"></param>
        public AllBecoViewModel ( BeckwithRepository becoRepository )
        {
            if ( becoRepository == null )
                throw new ArgumentNullException ( "becoRepository" );

            base.TabName = string.Format ( CultureInfo.InvariantCulture, "{0}:{1} ", becoRepository.UnitIPAddress, becoRepository.PortNumber );

            // copy repository to readonly repository.
            _becoRepository = becoRepository;

            // Subscribe for notifications of when a new customer is saved.
            _becoRepository.BecoAdded += this.OnBecoUnitAddedToRepository;

            // Populate the AllCustomers collection with CustomerViewModels.
            this.CreateAllBecoUnits ( );

            Task.Factory.StartNew ( ( ) => this.TestStarts ( ) );
        }

        private void CreateAllBecoUnits ( )
        {
            List<BecoViewModel> allViewModels =
                ( from becounits in _becoRepository.GetBecoUnits ( )
                  select new BecoViewModel ( becounits, _becoRepository ) ).ToList ( );

            foreach ( BecoViewModel cvm in allViewModels )
                cvm.PropertyChanged += this.OnBecoViewModelPropertyChanged;

            this.AllBecoUnits = new ObservableCollection<BecoViewModel> ( allViewModels );
            this.AllBecoUnits.CollectionChanged += this.OnCollectionChanged;
        }

        /// <summary>
        /// Starts automated tests.
        /// </summary>
        private void TestStarts ( )
        {
            Debug.WriteLine ( string.Format ( CultureInfo.InvariantCulture, "Test starts: {0}", DateTime.Now.TimeOfDay ) );

            // beco unit protocol values added by the user.
            ObservableCollection<BecoViewModel> testUnit = AllBecoUnits;

            // access modbus API.
            IModbus modbus = new Modbus ( );

            // Queue<Exception> parallelLoopExceptions = new Queue<Exception> ( );
            ParallelOptions parallelLoopOptions = new ParallelOptions
            {
                // CancellationToken = someTokeHere,
                MaxDegreeOfParallelism = Environment.ProcessorCount //,
                // TaskScheduler = someTaskScheduler
            };

            try
            {
                // TODO: Occasionally do not parallel some registers.
                ParallelLoopResult loopResult = Parallel.ForEach ( testUnit, parallelLoopOptions, ( testRegister, loopState ) =>
                    {
                        bool flag;

                        if ( Boolean.TryParse ( testRegister.Writable, out flag ) )
                        {
                            if ( flag )
                            {
                                // WRITE command.
                                modbus.Write ( testRegister );
                                Debug.WriteLine ( "Returned from WRITE" );

                            }
                            else
                            {
                                // Read command.
                                modbus.Read ( testRegister );

                                Debug.WriteLine ( "Returned from READ" );
                            }
                        }

                        // see if cancellation requested.
                        if ( loopState.ShouldExitCurrentIteration )
                            return;


                    }
                );

                if ( loopResult.IsCompleted )
                {
                    //string msg = string.Format (
                    //                        CultureInfo.InvariantCulture,
                    //                        "Testing completed: {0}",
                    //                        DateTime.Now.TimeOfDay
                    //                        );
                    Debug.WriteLine ( string.Format (
                                                    CultureInfo.InvariantCulture,
                                                    "Testing completed: {0}",
                                                    DateTime.Now.TimeOfDay
                                                    )
                                    );
                }

            }
            catch ( AggregateException e )
            {
                //string msg = string.Format (
                //                            CultureInfo.InvariantCulture,
                //                            Strings.Debug_ParallelingAggregateEx,
                //                            "AllBecoViewModel",
                //                            e
                //                            );
                Debug.WriteLine ( string.Format (
                                                CultureInfo.InvariantCulture,
                                                Strings.ParallelingAggregateEx,
                                                "AllBecoViewModel",
                                                e
                                                )
                                );
            }
            catch ( OperationCanceledException e )
            {
                //string msg = string.Format (
                //                            CultureInfo.InvariantCulture,
                //                            Strings.Debug_OperationCancelledEx,
                //                            "AllBecoViewModel",
                //                            e
                //                            );
                Console.WriteLine ( string.Format (
                                                    CultureInfo.InvariantCulture,
                                                    Strings.OperationCancelledEx,
                                                    "AllBecoViewModel",
                                                    e
                                                    )
                                  );
            }

        }

        #endregion // Constructor

        #region Public Interface

        /// <summary>
        /// Returns a collection of all the BecoViewModel objects.
        /// </summary>
        public ObservableCollection<BecoViewModel> AllBecoUnits { get; private set; }

        //public double TotalSelectedSales
        //{
        //    get
        //    {
        //        return this.AllCustomers.Sum(
        //            custVM => custVM.IsSelected ? custVM.t
        //    }
        //}

        #endregion // Public Interface

        #region Base Class Overrides

        protected override void OnDispose ( )
        {
            foreach ( BecoViewModel custVM in this.AllBecoUnits )
                custVM.Dispose ( );

            this.AllBecoUnits.Clear ( );
            this.AllBecoUnits.CollectionChanged -= this.OnCollectionChanged;

            _becoRepository.BecoAdded -= this.OnBecoUnitAddedToRepository;
        }

        #endregion // Base Class Overrides

        #region Event Handling Methods

        void OnCollectionChanged ( object sender, NotifyCollectionChangedEventArgs e )
        {
            if ( e.NewItems != null && e.NewItems.Count != 0 )
                foreach ( BecoViewModel custVM in e.NewItems )
                    custVM.PropertyChanged += this.OnBecoViewModelPropertyChanged;

            if ( e.OldItems != null && e.OldItems.Count != 0 )
                foreach ( BecoViewModel custVM in e.OldItems )
                    custVM.PropertyChanged -= this.OnBecoViewModelPropertyChanged;
        }

        void OnBecoViewModelPropertyChanged ( object sender, PropertyChangedEventArgs e )
        {
            //string IsSelected = "IsSelected";

            // Make sure that the property name we are referencing is valid.
            // This is a debugging technic, and does not execute in a Release build.
            ( sender as BecoViewModel ).VerifyPropertyName ( e.PropertyName );

            // When a beco unit is selected or unselected, we must let the
            // world know that the xxx property changed,
            // so that it will be queried again for a new value.
            // TODO: This might be useful to test individual test failures.
            //if ( e.PropertyName == IsSelected )
            //    this.OnPropertyChanged ( "TotalSelectedSales" );

            // Debug.WriteLine ( "AllBecoViewModel.OnBecoViewModelPropertyChanged is accessed. Following argument passed: " + e.PropertyName );
        }

        void OnBecoUnitAddedToRepository ( object sender, BecoUnitAddedEventArgs e )
        {
            Debug.WriteLine ( "OnBecoUnitAddedToRepository is running ..." );

            var viewModel = new BecoViewModel ( e.NewBecoUnit, _becoRepository );

            this.AllBecoUnits.Add ( viewModel );
        }

        #endregion // Event Handling Methods

    }
}
