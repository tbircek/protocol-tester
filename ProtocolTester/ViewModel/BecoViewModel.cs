﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using ProtocolTester.DataAccess;
using ProtocolTester.Model;
using ProtocolTester.Properties;
using System.Globalization;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;

namespace ProtocolTester.ViewModel
{
    /// <summary>
    /// A UI-friendly wrapper for a Beco object.
    /// </summary>
    public sealed class BecoViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        #region Fields

        readonly Beco _beco;
        readonly BeckwithRepository _becoRepository;

        // bool _isSelected;

        RelayCommand _saveCommand;

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Provides access to BecoViewModel.
        /// </summary>
        /// <param name="beco">provide single register information.</param>
        /// <param name="becoRepository">repository of beco values.</param>
        public BecoViewModel ( Beco beco, BeckwithRepository becoRepository )
        {
            if ( beco == null )
                throw new ArgumentNullException ( "beco" );

            if ( becoRepository == null )
                throw new ArgumentNullException ( "becoRepository" );

            _beco = beco;
            _becoRepository = becoRepository;

        }

        #endregion // Constructor

        #region Beco Properties

        /// <summary>
        /// IP Address of the unit user entered.
        /// </summary>
        public string IPAddress
        {
            get { return _beco.IPAddress; }
            set
            {
                if ( value == _beco.IPAddress )
                    return;

                DeviceId = "";
                _beco.IPAddress = value;

                TabName = _beco.IPAddress;

                base.OnPropertyChanged ( "DeviceId" );
                base.OnPropertyChanged ( "IPAddress" );
            }
        }

        /// <summary>
        /// PortNumber of the unit user entered.
        /// </summary>
        public string PortNumber
        {
            get { return _beco.PortNumber; }
            set
            {
                if ( value == _beco.PortNumber )
                    return;

                DeviceId = "";
                _beco.PortNumber = value;

                TabName = _beco.PortNumber;

                base.OnPropertyChanged ( "DeviceId" );
                base.OnPropertyChanged ( "PortNumber" );
            }
        }

        /// <summary>
        /// DeviceId of the unit read from the unit.
        /// </summary>
        public string DeviceId
        {
            get { return _beco.DeviceId; }
            set
            {
                if ( value == _beco.DeviceId )
                    return;

                _beco.DeviceId = value;
                base.OnPropertyChanged ( "DeviceId" );
            }
        }

        /// <summary>
        /// Register address value of the unit read from the xml file.
        /// </summary>
        public string Address
        {
            get { return _beco.Address; }
            set
            {
                if ( value == _beco.Address )
                    return;

                _beco.Address = value;
                base.OnPropertyChanged ( "Address" );
            }
        }

        /// <summary>
        /// Writable value of the unit read from the xml file.
        /// </summary>
        public string Writable
        {
            get { return _beco.Writable; }
            set
            {
                if ( value == _beco.Writable )
                    return;

                _beco.Writable = value;
                base.OnPropertyChanged ( "Writable" );
            }
        }

        /// <summary>
        /// Register accepted minimum value read from the xml file.
        /// </summary>
        public string Minimum
        {
            get { return _beco.Minimum; }
            set
            {
                if ( value == _beco.Minimum )
                    return;

                _beco.Minimum = value;
                base.OnPropertyChanged ( "Minimum" );
            }
        }

        /// <summary>
        /// Register accepted maximum value read from the xml file.
        /// </summary>
        public string Maximum
        {
            get { return _beco.Maximum; }
            set
            {
                if ( value == _beco.Maximum )
                    return;

                _beco.Maximum = value;
                base.OnPropertyChanged ( "Maximum" );
            }
        }

        /// <summary>
        /// Holds test completion percentage of the register range test.
        /// </summary>
        public int Percentage
        {
            get { return _beco.Percentage; }
            set
            {
                if ( value == _beco.Percentage )
                    return;

                _beco.Percentage = value;
                base.OnPropertyChanged ( "Percentage" );
            }
        }

        /// <summary>
        /// Holds information about if the test fail or pass.
        /// </summary>
        public string Result
        {
            get { return _beco.Result; }
            set
            {
                if ( value == _beco.Result )
                    return;

                _beco.Result = value;
                base.OnPropertyChanged ( "Result" );
            }
        }

        #endregion // Customer Properties

        #region Presentation Properties

        /// <summary>
        /// Returns the tab title.
        /// </summary>
        public override string TabName
        {
            get
            {
                return Strings.NewDevice; // _tabName;
            }
            //set
            //{
            //    if ( value == _tabName )
            //        return;

            //    _tabName = string.Format ( CultureInfo.InvariantCulture, Strings.NewTabTitle, IPAddress, ":", portNumber );

            //    base.OnPropertyChanged ( "TabName" );
            //}
        }

        ///// <summary>
        ///// Gets/sets whether this customer is selected in the UI.
        ///// </summary>
        //public bool IsSelected
        //{
        //    get { return _isSelected; }
        //    set
        //    {
        //        if ( value == _isSelected )
        //            return;

        //        _isSelected = value;

        //        base.OnPropertyChanged ( "IsSelected" );
        //    }
        //}

        /// <summary>
        /// Returns SaveCommand.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if ( _saveCommand == null )
                {
                    _saveCommand = new RelayCommand (
                        param => this.Save ( ),
                        param => this.CanSave
                            );
                }
                return _saveCommand;
            }
        }

        /// <summary>
        /// Saves the customer to the repository.
        /// This method is invoked by the SaveCommand
        /// </summary>
        public void Save ( )
        {
            if ( this.IsNewBecoUnit )
            {
                Debug.WriteLine ( "Save is running" );
                _becoRepository.AddBecoUnit ( _beco );

            }

            base.OnPropertyChanged ( "DisplayName" );
        }

        #endregion // Public Methods

        #region Private Helpers

        /// <summary>
        /// Returns true if this customer was created by the user and it has not yet
        /// been saved to the customer repository.
        /// </summary>
        bool IsNewBecoUnit
        {
            get
            {
                bool isNewBecoUnit = !_becoRepository.ContainsBecoUnit ( _beco );
                Debug.WriteLine ( "This is a new unit: " + isNewBecoUnit );

                return isNewBecoUnit;
            }
        }

        /// <summary>
        /// Enables or disables "Save" command.
        /// </summary>
        bool CanSave
        {
            get { return _beco.IsValid; }
        }

        #endregion // Private Helpers

        #region IDataErrorInfo Members

        /// <summary>
        /// 
        /// </summary>
        string IDataErrorInfo.Error
        {
            get { return ( _beco as IDataErrorInfo ).Error; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        string IDataErrorInfo.this[ string propertyName ]
        {
            get
            {

                Debug.WriteLine ( "BecoViewModel property: " + propertyName );

                string error = null;

                //// if ( propertyName == "CustomerType" )
                //if ( propertyName == "DeviceId" )
                //{
                //    // The IsCompany property of the Customer class
                //    // is Boolean, so it has no concept of being in
                //    // an "unselected" state. The CustomerViewModel
                //    // class handles this mapping and validation.
                //    // error = this.ValidateCustomerType ( );
                //    error = string.Empty;
                //}
                //else
                //{
                error = ( _beco as IDataErrorInfo )[ propertyName ];
                //}

                //  Custom commands registered with CommandMamager,
                // such as our Save command, so that they are queried
                // to see if they can execute now.
                CommandManager.InvalidateRequerySuggested ( );

                return error;
            }
        }

        #endregion
    }
}
