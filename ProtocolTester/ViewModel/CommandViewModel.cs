﻿using System;
using System.Windows.Input;

namespace ProtocolTester.ViewModel
{
    /// <summary>
    /// Represents an actionable item displayed by a View.
    /// </summary>
    public class CommandViewModel : ViewModelBase
    {
        public CommandViewModel ( string displayName, ICommand command )
        {
            if ( command == null )
                throw new ArgumentNullException ( "command" );

            base.DisplayName = displayName;
            // base.TabName = displayName;
            this.Command = command;
        }

        public ICommand Command { get; private set; }
    }
}
