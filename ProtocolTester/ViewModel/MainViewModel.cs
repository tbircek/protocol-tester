﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using ProtocolTester.DataAccess;
using ProtocolTester.Model;
using ProtocolTester.Properties;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;
using System.Globalization;

namespace ProtocolTester.ViewModel
{
    /// <summary>
    /// The ViewModel for the application's main window.
    /// </summary>
    public class MainViewModel : WorkspaceViewModel
    {
        #region Fields

        bool StartTestEnable { get; set; }

        public bool StopTestEnable { get; set; }

        ReadOnlyCollection<CommandViewModel> _commands;
        readonly BeckwithRepository _becoRepository;
        ObservableCollection<WorkspaceViewModel> _workspaces;

        #endregion // Fields

        #region Constructor

        //public MainViewModel ( string becoDataFile )
        public MainViewModel ( )
        {
            base.DisplayName =
                string.Format ( CultureInfo.InvariantCulture,
                                Strings.ProgramTitle,
                                typeof ( MainViewModel ).Assembly.GetName ( ).Version
                               );

            // _becoRepository = new BeckwithRepository ( becoDataFile );

            _becoRepository = new BeckwithRepository ( );
            // _becoPDFRepository = new BeckwithRepositoryPDF ( becoDataFile );

            CreateNewBecoUnit ( );
                        
        }

        //public MainViewModel ( )
        //{
        // GeneralInvoceTypes = new CommOptionsViewModel ( );
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public ObservableCollection<string> ProductImages { get; set; }
        //{
        //    get
        //    {
        //        var results = new ObservableCollection<string> ( );

        //        // results.Add ( null );
        //        //results.Add ( "Images\\M-2001D.jpg" );
        //        //results.Add ( "Images\\M-6200A.jpg" );
        //        //results.Add ( "Images\\M-6280A.jpg" );

        //        return results;
        //    }
        //}

        #endregion // Presentation Properties

        #region Public Methods

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="fileName"></param>
        ///// <returns></returns>
        //public ImageSource LoadImage ( string fileName )
        //{
        //    var image = new BitmapImage ( );

        //    using ( var stream = new FileStream ( fileName, FileMode.Open ) )
        //    {
        //        image.BeginInit ( );
        //        image.CacheOption = BitmapCacheOption.OnLoad;
        //        image.StreamSource = stream;
        //        image.EndInit ( );

        //    }
        //    return image;
        //}

        #endregion // Constructor

        #region Commands

        /// <summary>
        /// Returns read-only list of commands
        /// that the UI can display and execute.
        /// </summary>
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                //if ( _becoRepository.GetBecoUnits ( ).Count > 0 )
                //{
                if ( _commands == null )
                {
                    List<CommandViewModel> cmds = this.CreateCommands ( );
                    _commands = new ReadOnlyCollection<CommandViewModel> ( cmds );
                }
                //}

                return _commands;
            }
        }

        List<CommandViewModel> CreateCommands ( )
        {
            return new List<CommandViewModel>
            {                
                // establishes a new command to "add a new device".
                new CommandViewModel(
                    Strings.NewDevice,
                    new RelayCommand( param=> this.CreateNewBecoUnit ( ), 
                                      param=> this.StartTestEnable )),

                // establishes a new command for start a test.
                // initially there is no Beco Units available to show.
                // So disable this command until the user adds a Beco Unit.
                new CommandViewModel ( 
                    Strings.StartTest,
                    new RelayCommand(param=> this.ShowAllBecoUnits ( ),
                                     param=> _becoRepository.GetBecoUnits( ).Count > 0 )),   
                                     // param => this.StopTestEnable )),
                
                // establishes a new command to "stop test".
                new CommandViewModel(
                    Strings.StopTest,
                    new RelayCommand(param=> this.CreateNewBecoUnit( ), 
                                     param => this.StopTestEnable ))
            };
        }

        #endregion // Commands

        #region Workspaces

        /// <summary>
        /// Returns the collection of available workspaces to display.
        /// A 'workspace is a ViewModel that can request to be closed.
        /// </summary>
        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if ( _workspaces == null )
                {
                    _workspaces = new ObservableCollection<WorkspaceViewModel> ( );
                    _workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return _workspaces;
            }
        }

        void OnWorkspacesChanged ( object sender, NotifyCollectionChangedEventArgs e )
        {
            Debug.WriteLine ( "OnWorkspacesChanged" );

            if ( e.NewItems != null && e.NewItems.Count != 0 )
                foreach ( WorkspaceViewModel workspace in e.NewItems )
                {
                    workspace.RequestClose += this.OnWorkspaceRequestClose;
                    // "do not allow any more tab title = "Add a new device".
                    if ( workspace.TabName == Strings.NewDevice )
                        StartTestEnable = false;
                    else
                        StopTestEnable = true;
                }

            if ( e.OldItems != null && e.OldItems.Count != 0 )
                foreach ( WorkspaceViewModel workspace in e.OldItems )
                {
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
                    // "allow tab title = "Add a new device".                    
                    if ( workspace.TabName == Strings.NewDevice )
                    {
                        StartTestEnable = true;
                        StopTestEnable = false;
                    }
                }
        }

        void OnWorkspaceRequestClose ( object sender, EventArgs e )
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose ( );
            this.Workspaces.Remove ( workspace );
        }

        #endregion // Workspaces

        #region Private Helpers

        void CreateNewBecoUnit ( )
        {
            Beco newBeco = Beco.CreateNewBecoUnit ( );
            BecoViewModel workspace = new BecoViewModel ( newBeco, _becoRepository );
            this.Workspaces.Add ( workspace );
            this.SetActiveWorkspace ( workspace );
        }

        void ShowAllBecoUnits ( )
        {
            AllBecoViewModel workspace =
                this.Workspaces.FirstOrDefault ( vm => vm is AllBecoViewModel )
                as AllBecoViewModel;

            if ( workspace == null )
            {
                workspace = new AllBecoViewModel ( _becoRepository );
                this.Workspaces.Add ( workspace );
            }
            this.SetActiveWorkspace ( workspace );

            // remove "Add a new device" tab to preventmultiple unit entries.
            BecoViewModel someOtherWorkspace =
                this.Workspaces.FirstOrDefault ( vm => vm is BecoViewModel )
                as BecoViewModel;

            this.Workspaces.Remove ( someOtherWorkspace );

            // disable addition of "Add a new device" tab.
            StartTestEnable = false;
        }

        void SetActiveWorkspace ( WorkspaceViewModel workspace )
        {
            Debug.Assert ( this.Workspaces.Contains ( workspace ) );

            ICollectionView collectionView = CollectionViewSource.GetDefaultView ( this.Workspaces );
            if ( collectionView != null )
                collectionView.MoveCurrentTo ( workspace );
        }

        #endregion // Private Helpers
    }
}
