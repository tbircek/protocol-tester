﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ProtocolTester.ViewModel
{
    /// <summary>
    /// This ViewModelBase subclass requests to be removed
    /// from the UI when its CloseCommand executes.
    /// This class is abstract.
    /// </summary>
    public abstract class WorkspaceViewModel : ViewModelBase
    {
        #region Fields

        RelayCommand _closeCommand;

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Default WorkspaceViewModel.
        /// </summary>
        protected WorkspaceViewModel ( ) 
        { 
        }

        #endregion // Constructor

        #region CloseCommand

        /// <summary>
        /// Provides Close command throughout the project.
        /// </summary>
        public ICommand CloseCommand
        {
            get
            {
                if ( _closeCommand == null )
                    _closeCommand = new RelayCommand ( param => this.OnRequestClose ( ) );

                return _closeCommand;
            }
        }

        #endregion // CloseCommand

        #region RequestClose [event]

        /// <summary>
        /// Raised when this workspace should be removed from the UI.
        /// </summary>
        public event EventHandler RequestClose;

        void OnRequestClose ( )
        {
            EventHandler handler = this.RequestClose;
            if ( handler != null )
                handler ( this, EventArgs.Empty );
        }

        #endregion // RequestClose [event]
    }
}
